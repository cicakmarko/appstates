//
// Created by Marko Cicak on 3/17/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

import Foundation

class AppStateActive: AppState {
    
    override func enterState() {
        super.enterState()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
}

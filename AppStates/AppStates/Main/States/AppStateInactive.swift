//
// Created by Marko Cicak on 3/17/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

import Foundation

class AppStateInactive: AppState {
    
    override func enterState() {
        super.enterState()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
}

class AppStateWakeUp: AppStateInactive {
    
    override func enterState() {
        super.enterState()
        // undo any changes made when entering the background
    }
}

class AppStateResignActive: AppStateInactive {
    
    override func enterState() {
        super.enterState()
        // undo any changes made when entering the background
    }
}

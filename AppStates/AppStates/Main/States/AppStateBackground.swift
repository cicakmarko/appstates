//
// Created by Marko Cicak on 3/17/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

import Foundation
import UIKit

class AppStateBackground : AppState {
    
    override func enterState() {
        super.enterState()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    override func leaveState() {
        super.leaveState()
        // can leave to terminated or inactive
        
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
}

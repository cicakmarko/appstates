//
// Created by Marko Cicak on 3/17/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

import Foundation
import UIKit

class AppState {
    
    func enterState() {
        let c = UIApplication.sharedApplication().delegate?.window!?.rootViewController as! ViewController
        c.label.text = "\(self.dynamicType)"
        print("enter state \(self.dynamicType)\n")
    }
    
    func leaveState() {
        print("leave state \(self.dynamicType)")
    }
}

//
// Created by Marko Cicak on 3/17/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

import Foundation

class AppStateInit: AppState {
    
    func willFinishLaunchingWithOptions() -> Bool {
        print("willFinishLaunchingWithOptions")
        return true
    }
    
    func didFinishLaunchingWithOptions() -> Bool {
        // Override point for customization after application launch.
        print("didFinishLaunchingWithOptions")
        return true
    }
}

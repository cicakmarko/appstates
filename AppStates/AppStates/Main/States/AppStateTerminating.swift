//
// Created by Marko Cicak on 3/17/16.
// Copyright (c) 2016 codecentric. All rights reserved.
//

import Foundation

class AppStateTerminating: AppState {
    
    override func enterState() {
        super.enterState()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("TERMINATING")
    }
}

//
//  AppDelegate.swift
//  AppStates
//
//  Created by Marko Cicak on 3/17/16.
//  Copyright © 2016 codecentric. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var _currentState: AppState = AppStateInit()
    var currentState: AppState {
        get {
            return _currentState
        }
        set(newVal) {
            _currentState.leaveState()
            _currentState = newVal
            _currentState.enterState()
        }
    }
    
    lazy var controller : ViewController = {
        return self.window!.rootViewController as! ViewController
    }()
    
    func application(application: UIApplication, willFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
        return (currentState as! AppStateInit).willFinishLaunchingWithOptions()
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject:AnyObject]?) -> Bool {
        return (currentState as! AppStateInit).didFinishLaunchingWithOptions()
    }

    func applicationWillResignActive(application: UIApplication) {
        currentState = AppStateResignActive()
    }

    func applicationDidEnterBackground(application: UIApplication) {
        currentState = AppStateBackground()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        currentState = AppStateWakeUp()
    }

    func applicationDidBecomeActive(application: UIApplication) {
        currentState = AppStateActive()
    }

    func applicationWillTerminate(application: UIApplication) {
        currentState = AppStateTerminating()
    }
}
